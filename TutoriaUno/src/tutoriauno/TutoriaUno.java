/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tutoriauno;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class TutoriaUno {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero = 10; //Directamente, variable, retorno de un método
        int numero2 = numero;
        System.out.print("Digite un número entero: ");
        int numero3 = sc.nextInt();
        System.out.println("Hola Mundo!!");

        //Estructuras de Control IF
        if (numero3 == 10) {
            //Bloque de codigo cuando es verdadero
            System.out.println("Ejectuar las líneas de código...");
        } else if (numero3 >= 7 && numero3 <= 9) {
            //Bloque de codigo cuando es verdadero, y no se cumplio la anterior 
            System.out.println("Ejecutar las líneas de este bloque. ");
        } else {
            //Bloque de codigo cuando es falso
            System.out.println("Si es falso, entro aquí");
        }
        System.out.println("Gracias por utilizar la aplicación.");

        System.out.println("\nEjemplo del cajero electrónico\n");
        int monto = 6100;
        //2000, 1000, 500, 100
        //2x2000, 1x1000, 1x500, 1x100
        if (monto >= 2000) {
            System.out.println(monto / 2000 + "x2000"); 
            monto %= 2000;
//            5600|2000
//            4000 2  
//            ----
//            1600
//            monto -= (monto / 2000) * 2000;
        }
        if (monto >= 1000) {
            System.out.println(monto / 1000 + "x1000");
            monto %= 1000;
        }
        if (monto >= 500) {
            System.out.println(monto / 500 + "x500");
            monto %= 500;
        }
        if (monto >= 100) {
            System.out.println(monto / 100 + "x100");
            monto %= 100;
        }

        switch (numero3) {
            case 1:
                System.out.println("Cuando es uno");
                System.out.println("Cuando es uno");
                System.out.println("Cuando es uno");
                System.out.println("Cuando es uno");
                System.out.println("Cuando es uno");
                break;
            case 2:
                System.out.println("Cuando la variable es 2");
                System.out.println("Cuando la variable es 2");
                System.out.println("Cuando la variable es 2");
                System.out.println("Cuando la variable es 2");
                System.out.println("Cuando la variable es 2");
                break;
            default:
                System.out.println("Cuando no es ninguna de las anteriores, y es opcional");
        }

        System.out.println("\nEjemplo de calculadora\n");
        int num1 = 10;
        int num2 = 20;
        sc = new Scanner(System.in);
        char operacion = sc.nextLine().charAt(0);

        switch (operacion) {
            case '+':
                System.out.println(num1 + num2);
                break;
            case '-':
                System.out.println(num1 - num2);
                break;
            case '*':
                System.out.println(num1 * num2);
                break;
            default:
                System.out.println("Operación no soportada.");

        }

        System.out.println("Monto: " + monto);

    }

}
