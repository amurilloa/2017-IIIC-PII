/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpuzzle.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import labpuzzle.entities.MiError;
import labpuzzle.entities.Usuario;

/**
 *
 * @author ALLAN
 */
public class UsuarioDAO {
    
    public boolean autentificar(Usuario usuario) {

        //Si esta el mae y contraseña esta bien, return true
        //Si el mae esta y esta mal, return false
        //Registrar el mae, return true
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from usuario where usuario = ? "
                    + "and password = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usuario.getUsuario());
            stmt.setString(2, usuario.getPassword());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                usuario.setId(rs.getInt("id"));
                return true;
            }
            sql = "select * from usuario where usuario = ?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, usuario.getUsuario());
            rs = stmt.executeQuery();
            if (rs.next()) {
                throw new MiError("Usuario ya existe, o contraseña inválida");
            }
            
            sql = "insert into usuario(usuario, password) values(?, ?)";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, usuario.getUsuario());
            stmt.setString(2, usuario.getPassword());
            return stmt.executeUpdate() > 0;
            
        } catch (SQLException ex) {
            throw new MiError(ex.getMessage());
        }
    }
    
}
