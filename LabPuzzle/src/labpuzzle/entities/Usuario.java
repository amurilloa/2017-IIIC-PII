/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpuzzle.entities;

import java.util.Date;

/**
 *
 * @author ALLAN
 */
public class Usuario {

    private int id;
    private String usuario;
    private String password;
    private int tiempo;
    private Date fechaRegistro;

    public Usuario() {
    }

    public Usuario(int id, String usuario, String password, int tiempo, Date fechaRegistro) {
        this.id = id;
        this.usuario = usuario;
        this.password = password;
        this.tiempo = tiempo;
        this.fechaRegistro = fechaRegistro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", usuario=" + usuario + ", password=" + password + ", tiempo=" + tiempo + ", fechaRegistro=" + fechaRegistro + '}';
    }

}
