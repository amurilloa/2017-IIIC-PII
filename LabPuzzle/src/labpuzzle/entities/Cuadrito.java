/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpuzzle.entities;

import javax.swing.ImageIcon;

/**
 *
 * @author ALLAN
 */
public class Cuadrito {

    private int numero;
    private ImageIcon imagen;

    public Cuadrito() {
    }

    public Cuadrito(int numero, ImageIcon imagen) {
        this.numero = numero;
        this.imagen = imagen;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "Cuadrito{" + "numero=" + numero + ", imagen=" + imagen + '}';
    }

}
