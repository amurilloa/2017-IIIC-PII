/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpuzzle.bo;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import labpuzzle.entities.Cuadrito;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private BufferedImage imagen;
    private int tiempo;
    private Cuadrito[][] cuadritos;

    public Logica() {

    }

    public void config(int w, int h) {
        int num = 1;
        cuadritos = new Cuadrito[4][4];
        for (int f = 0; f < cuadritos.length; f++) {
            for (int c = 0; c < cuadritos[f].length; c++) {
                if (num == 16) {
                    break;
                }
                int x = f * w;
                int y = c * h;
                cuadritos[f][c] = new Cuadrito();
                cuadritos[f][c].setNumero(num++);
                cuadritos[f][c].setImagen(getPieza(y, x, w, h));
            }
        }
        revolver();
    }

    public ImageIcon getPieza(int x, int y, int w, int h) {
        return new ImageIcon(imagen.getSubimage(x, y, w, h));
    }

    public String aumetarTiempo() {
        tiempo++;
        int s = tiempo % 60;
        int m = tiempo / 60;
        int h = m / 60;
        m %= 60;

        return (h < 10 ? "0" + h : h) + ":"
                + (m < 10 ? "0" + m : m) + ":"
                + (s < 10 ? "0" + s : s);
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public BufferedImage getImagen() {
        return imagen;
    }

    public void setImagen(BufferedImage imagen) {
        this.imagen = imagen;
    }

    public Cuadrito[][] getCuadritos() {
        return cuadritos;
    }

    public void mover(String coor) {
        String[] coord = coor.split(",");
        int f = Integer.parseInt(coord[0]);
        int c = Integer.parseInt(coord[1]);
        if (c < 3 && cuadritos[f][c + 1] == null) {
            cuadritos[f][c + 1] = cuadritos[f][c];
            cuadritos[f][c] = null;
        } else if (c > 0 && cuadritos[f][c - 1] == null) {
            cuadritos[f][c - 1] = cuadritos[f][c];
            cuadritos[f][c] = null;
        } else if (f < 3 && cuadritos[f + 1][c] == null) {
            cuadritos[f + 1][c] = cuadritos[f][c];
            cuadritos[f][c] = null;
        } else if (f > 0 && cuadritos[f - 1][c] == null) {
            cuadritos[f - 1][c] = cuadritos[f][c];
            cuadritos[f][c] = null;
        }
    }

    public boolean revisar() {
        int cont = 1;
        for (int i = 0; i < cuadritos.length; i++) {
            for (int j = 0; j < cuadritos[i].length; j++) {
                if (cuadritos[i][j] != null && cuadritos[i][j].getNumero() != cont) {
                    return false;
                }
                cont++;
            }
        }
        return true;
    }

    public void revolver() {
        for (int i = 0; i < 10000; i++) {
            int j = (int) (Math.random() * 4);
            int k = (int) (Math.random() * 4);
            if (cuadritos[j][k] != null) {
                mover(j + "," + k);
            }
        }
        int con = 0;
        while (cuadritos[3][3] != null) {
            con++;
            System.out.println(con);
            int j = (int) (Math.random() * 4);
            int k = (int) (Math.random() * 4);
            if (cuadritos[j][k] != null) {
                mover(j + "," + k);
            }
        }
    }
}
