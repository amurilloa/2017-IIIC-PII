/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpuzzle.bo;

import labpuzzle.dao.UsuarioDAO;
import labpuzzle.entities.MiError;
import labpuzzle.entities.Usuario;

/**
 *
 * @author ALLAN
 */
public class UsuarioBO {

    public boolean login(Usuario usuario) {
        if (usuario.getUsuario().isEmpty()) {
            throw new MiError("Favor digite el nombre de usuario");
        }

        if (usuario.getPassword().isEmpty()) {
            throw new MiError("Favor digite una contraseña válida");
        }

        usuario.setPassword(encriptar(usuario.getPassword()));
        UsuarioDAO udao = new UsuarioDAO();
        return udao.autentificar(usuario);

    }

    private String encriptar(String password) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(password.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new MiError("Favor digitar una nueva contraseña");
        }
    }

}
