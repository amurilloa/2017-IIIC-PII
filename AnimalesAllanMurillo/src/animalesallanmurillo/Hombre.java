/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Hombre extends Humano {

    public Hombre() {
    }

    public Hombre(String cedula, String nombre, String apellidoUno, String apellidoDos, String pelaje, int periodoGestacional, String genero, String color, float peso, float talla) {
        super(cedula, nombre, apellidoUno, apellidoDos, pelaje, periodoGestacional, genero, color, peso, talla);
    }

    @Override
    public String toString() {
        return "Hombre{" + '}';
    }
    
}
