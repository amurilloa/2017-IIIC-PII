/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Mamifero extends Animal{
    private String pelaje;
    private int periodoGestacional;

    public Mamifero() {
    }
    
    public Mamifero(String pelaje, int periodoGestacional, String genero, String color, float peso, float talla) {
        super(genero, color, peso, talla);
        this.pelaje = pelaje;
        this.periodoGestacional = periodoGestacional;
    }

    public String getPelaje() {
        return pelaje;
    }

    public void setPelaje(String pelaje) {
        this.pelaje = pelaje;
    }

    public int getPeriodoGestacional() {
        return periodoGestacional;
    }

    public void setPeriodoGestacional(int periodoGestacional) {
        this.periodoGestacional = periodoGestacional;
    }

    @Override
    public String toString() {
        return "Mamifero{" + "pelaje=" + pelaje + ", periodoGestacional=" + periodoGestacional + '}';
    }
    
    
}
