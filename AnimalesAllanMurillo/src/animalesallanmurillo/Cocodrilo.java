/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Cocodrilo extends Reptil {

    private String especie;

    public Cocodrilo() {
    }

    public Cocodrilo(String especie, boolean escamas, boolean autotomiaCaudal, String genero, String color, float peso, float talla) {
        super(escamas, autotomiaCaudal, genero, color, peso, talla);
        this.especie = especie;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    @Override
    public String toString() {
        return "Cocodrilo{" + "especie=" + especie + '}';
    }
    
    

}
