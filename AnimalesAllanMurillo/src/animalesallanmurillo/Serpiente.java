/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Serpiente extends Reptil {

    private Boolean venenosa;
    private String tipo;

    public Serpiente() {
    }

    public Serpiente(Boolean venenosa, String tipo, boolean escamas, boolean autotomiaCaudal, String genero, String color, float peso, float talla) {
        super(escamas, autotomiaCaudal, genero, color, peso, talla);
        this.venenosa = venenosa;
        this.tipo = tipo;
    }

    public Boolean getVenenosa() {
        return venenosa;
    }

    public void setVenenosa(Boolean venenosa) {
        this.venenosa = venenosa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Serpiente{" + "venenosa=" + venenosa + ", tipo=" + tipo + '}';
    }

}
