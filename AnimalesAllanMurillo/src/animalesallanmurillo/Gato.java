/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Gato  extends Mamifero {
    private int pedigri;
    private String nombre;
    private String raza;

    public Gato() {
    }

    public Gato(int pedigri, String nombre, String raza, String pelaje, int periodoGestacional, String genero, String color, float peso, float talla) {
        super(pelaje, periodoGestacional, genero, color, peso, talla);
        this.pedigri = pedigri;
        this.nombre = nombre;
        this.raza = raza;
    }


    public int getPedigri() {
        return pedigri;
    }

    public void setPedigri(int pedigri) {
        this.pedigri = pedigri;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public String toString() {
        return "Gato{" + "pedigri=" + pedigri + ", nombre=" + nombre + ", raza=" + raza + '}';
    }
    
    
}
