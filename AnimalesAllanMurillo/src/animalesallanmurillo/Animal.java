/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
    public class Animal {
    
    private String genero;
    private String color;
    private float peso;
    private float talla;

    public Animal() {
    }

    public Animal(String genero, String color, float peso, float talla) {
        this.genero = genero;
        this.color = color;
        this.peso = peso;
        this.talla = talla;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getTalla() {
        return talla;
    }

    public void setTalla(float talla) {
        this.talla = talla;
    }

    @Override
    public String toString() {
        return "Animal{" + "genero=" + genero + ", color=" + color + ", peso=" + peso + ", talla=" + talla + '}';
    }
    
}
