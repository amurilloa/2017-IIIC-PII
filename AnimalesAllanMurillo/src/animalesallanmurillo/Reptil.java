/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Reptil extends Animal {

    private boolean escamas;
    private boolean autotomiaCaudal;

    public Reptil() {
    }

    public Reptil(boolean escamas, boolean autotomiaCaudal, String genero, String color, float peso, float talla) {
        super(genero, color, peso, talla);
        this.escamas = escamas;
        this.autotomiaCaudal = autotomiaCaudal;
    }

    public boolean isEscamas() {
        return escamas;
    }

    public void setEscamas(boolean escamas) {
        this.escamas = escamas;
    }

    public boolean isAutotomiaCaudal() {
        return autotomiaCaudal;
    }

    public void setAutotomiaCaudal(boolean autotomiaCaudal) {
        this.autotomiaCaudal = autotomiaCaudal;
    }

    @Override
    public String toString() {
        return "Reptil{" + "escamas=" + escamas + ", autotomiaCaudal=" + autotomiaCaudal + '}';
    }

}
