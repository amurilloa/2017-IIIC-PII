/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Humano extends Mamifero {

    private String cedula;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;

    public Humano() {
    }

    public Humano(String cedula, String nombre, String apellidoUno, String apellidoDos, String pelaje, int periodoGestacional, String genero, String color, float peso, float talla) {
        super(pelaje, periodoGestacional, genero, color, peso, talla);
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    @Override
    public String toString() {
        return "Humano{" + "cedula=" + cedula + ", nombre=" + nombre + ", apellidoUno=" + apellidoUno + ", apellidoDos=" + apellidoDos + '}';
    }

}
