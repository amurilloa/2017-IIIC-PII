/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animalesallanmurillo;

import java.util.LinkedList;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private LinkedList<Animal> animales;

    public Logica() {
        this.animales = new LinkedList<>();
        cargarDatos();
    }

    /**
     * Agrega un animal a la lista correspondiente
     *
     * @param animal Animal que se desea agregar a la lista.
     */
    public void registrarAnimal(Animal animal) {
        animales.add(animal);
    }

    public LinkedList<Animal> getAnimales() {
        return animales;
    }

    private void cargarDatos() {
        animales.add(new Animal());
        animales.add(new Mamifero());
        animales.add(new Mamifero());
        animales.add(new Mamifero());
        animales.add(new Perro());
        animales.add(new Perro());
        animales.add(new Perro());
        animales.add(new Gato());
        animales.add(new Gato());
        animales.add(new Gato());
        animales.add(new Gato());
        animales.add(new Gato());
        animales.add(new Hombre());
        animales.add(new Hombre());
        animales.add(new Hombre());
        animales.add(new Hombre());
        animales.add(new Mujer());
        animales.add(new Mujer());
        animales.add(new Mujer());
        animales.add(new Mujer());
        animales.add(new Mujer());
        animales.add(new Reptil());
        animales.add(new Serpiente());
        animales.add(new Serpiente());
        animales.add(new Cocodrilo());
        animales.add(new Cocodrilo());
        animales.add(new Cocodrilo());
    }

    public DefaultPieDataset getGraficoAnimales() {
        DefaultPieDataset data = new DefaultPieDataset();
        int cantMami = 0;
        int cantRept = 0;
        for (Animal a : animales) {
            if (a instanceof Mamifero) {
                cantMami++;
            }
            if (a instanceof Reptil) {
                cantRept++;
            }
        }
        data.setValue("Mamiferos", cantMami);
        data.setValue("Reptiles", cantRept);
        return data;
    }

    public DefaultPieDataset getGraficoMamiferos() {
        DefaultPieDataset data = new DefaultPieDataset();
        int cantPerros = 0;
        int cantGatos = 0;
        int cantHumanos = 0;
        for (Animal a : animales) {
            if (a instanceof Perro) {
                cantPerros++;
            }
            if (a instanceof Gato) {
                cantGatos++;
            }
            if (a instanceof Humano) {
                cantHumanos++;
            }
        }
        data.setValue("Perros", cantPerros);
        data.setValue("Gatos", cantGatos);
        data.setValue("Humanos", cantHumanos);
        return data;
    }

    public DefaultPieDataset getGraficoHumanos() {
        DefaultPieDataset data = new DefaultPieDataset();
        int cantH = 0;
        int cantM = 0;
        for (Animal a : animales) {
            if (a instanceof Hombre) {
                cantH++;
            }
            if (a instanceof Mujer) {
                cantM++;
            }
        }
        data.setValue("Hombres", cantH);
        data.setValue("Mujeres", cantM);
        return data;
    }

    public DefaultPieDataset getGraficoReptiles() {
        DefaultPieDataset data = new DefaultPieDataset();
        int cantSer = 0;
        int cantCoc = 0;
        for (Animal a : animales) {
            if (a instanceof Cocodrilo) {
                cantCoc++;
            }
            if (a instanceof Serpiente) {
                cantSer++;
            }
        }
        data.setValue("Serpientes", cantSer);
        data.setValue("Cocodrilo", cantCoc);
        return data;
    }

}
