/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package car;

/**
 *
 * @author ALLAN
 */
public class Persona {

    private String nombre;
    private Corazon corazon;
    private Coche coche;

    public Persona(String nombre) {
        this.nombre = nombre;
        this.corazon = new Corazon();
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }

    public void tranquiliza() {
        if (corazon.leerRitmo() - 5 >= 0) {
            corazon.cambiaRitmo(corazon.leerRitmo() - 5);
        }
    }
    
    

}
