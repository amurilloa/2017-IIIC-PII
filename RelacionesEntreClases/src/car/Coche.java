/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package car;

/**
 *
 * @author ALLAN
 */
public class Coche {

    private Motor motor;
    private Persona conductor;

    public Coche(Motor motor) {
        this.motor = motor;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public void enciende() {
        if (motor != null && !motor.isActivo()) {
            motor.activa();
            motor.cambiaRpm(800);
            Sound.play("carstart2.wav");
        }
    }

    public void apaga() {
        if (motor != null && motor.isActivo()) {
            motor.cambiaRpm(0);
            motor.desactiva();
        }
    }
    
    public void acelera(){
        if(motor != null && motor.isActivo()) {
            motor.cambiaRpm(500);
            Sound.play("car_accelerating_2.wav");
        }
    }

}
