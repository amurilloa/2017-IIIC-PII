/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package car;

import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 *
 * @author ALLAN
 */
public class Sound {

    public static void play(String file) {
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(new File(file));
            try (Clip test = AudioSystem.getClip()) {
                test.open(ais);
                test.start();
                
                while (!test.isRunning()) {
                    Thread.sleep(10);
                }
                while (test.isRunning()) {
                    Thread.sleep(10);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
