/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private String frase;

    /**
     *
     * @return
     */
    public char[] stringToArray() {
        return frase.toCharArray();
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }

    public String getFrase() {
        return frase;
    }

    /**
     *
     * @return
     */
    public char[][] matriz() {
        String aux = frase.toLowerCase()
                .replaceAll("[a-z&&[^aeiou]]", "0")
                .replaceAll("[aeiou]", "x")
                .replaceAll("[0]", "o");
        char[][] matriz = {frase.toCharArray(), aux.toCharArray()};
        return matriz;
    }

    /**
     *
     * @param frase
     * @return
     */
    public String imprimir(char[] frase) {
        String nueva = "";
        for (char letra : frase) {
            nueva += letra + "|";
        }
        return nueva += "\b";
    }

    /**
     *
     * @param matriz
     * @return
     */
    public String imprimirM(char[][] matriz) {
        String tex = "";
        for (int i = 0; i < matriz.length; i++) {
            tex += imprimir(matriz[i]) + "\n";
        }
        return tex;
    }
}
