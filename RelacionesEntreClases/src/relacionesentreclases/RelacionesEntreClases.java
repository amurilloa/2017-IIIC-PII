/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreclases;

import java.util.Date;

/**
 *
 * @author ALLAN
 */
public class RelacionesEntreClases {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cuenta cu = new Cuenta(12312312l, 0.03, null);
        Cliente cl = new Cliente("Allan", "Murillo Alfaro", "50m oeste de la escuela", "Chachagua", new Date());
        cu.setTitular(cl);
        System.out.println(cu.leerTitular().nombreCompleto());
        System.out.println(cu.leerTitular().direccionCompleta());
        cu.ingreso(1000);
        System.out.println(cu.enRojos());
        
        Poligono p = new Poligono();
        p.agregarSegmento(new Segmento(10));
        p.agregarSegmento(new Segmento(11));
        p.agregarSegmento(new Segmento(12));
        p.agregarSegmento(new Segmento(13));
        System.out.println(p);
        
        
    }

}
