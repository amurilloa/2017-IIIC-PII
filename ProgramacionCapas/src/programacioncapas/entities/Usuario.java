/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.entities;

/**
 *
 * @author ALLAN
 */
public class Usuario {

    private int id;
    private String email;
    private String contrasena;
    private int telefono;

    public Usuario() {
    }

    public Usuario(int id, String email, String contrasena, int telefono) {
        this.id = id;
        this.email = email;
        this.contrasena = contrasena;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", email=" + email + ", contrasena=" + contrasena + ", telefono=" + telefono + '}';
    }
}
