/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import programacioncapas.entities.MiError;
import programacioncapas.entities.Usuario;

/**
 *
 * @author ALLAN
 */
public class UsuarioDAO {

    public boolean insertar(Usuario usuario) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into usuario(email, pass, telefono) "
                    + "values (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usuario.getEmail());
            stmt.setString(2, usuario.getContrasena());
            stmt.setInt(3, usuario.getTelefono());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());
            throw new MiError("No se pudo registrar el usuario, favor intente nuevamente");
        }
    }

    public LinkedList<Usuario> cargarTodo() {
        LinkedList<Usuario> usuarios = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from usuario";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                usuarios.add(cargarUsuario(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los usuarios, favor intente nuevamente");
        }

        return usuarios;
    }

    private Usuario cargarUsuario(ResultSet rs) throws SQLException {
        Usuario u = new Usuario();
        u.setId(rs.getInt("id"));
        u.setEmail(rs.getString("email"));
        u.setContrasena(rs.getString("pass"));
        u.setTelefono(rs.getInt("telefono"));
        return u;
    }

    public Usuario cargarID(int id) {
        Usuario usuario = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from usuario where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                usuario = cargarUsuario(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los usuarios, favor intente nuevamente");
        }

        return usuario;
    }

    public boolean modificar(Usuario usuario) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update usuario set email=?, pass=?, telefono=?"
                    + " where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, usuario.getEmail());
            stmt.setString(2, usuario.getContrasena());
            stmt.setInt(3, usuario.getTelefono());
            stmt.setInt(4, usuario.getId());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());
            throw new MiError("No se pudo modificar el usuario, favor intente nuevamente");
        }
    }

    public boolean eliminar(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "delete from usuario where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());
            throw new MiError("No se pudo modificar el usuario, favor intente nuevamente");
        }
    }

    public LinkedList<Usuario> cargarFiltro(String filtro) {
        LinkedList<Usuario> usuarios = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from usuario where email like ? "
                    + " or cast(telefono as text) like ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, filtro + '%');
            stmt.setString(2, filtro + '%');
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                usuarios.add(cargarUsuario(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los usuarios, favor intente nuevamente");
        }

        return usuarios;
    }

    public boolean validar(String email, String pass) {

        Usuario usuario = null;

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from usuario where email = ? and pass = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setString(2, pass);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                usuario = cargarUsuario(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los usuarios, favor intente nuevamente");
        }

        return usuario != null;
    }
}
