/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacioncapas.bo;

import java.util.LinkedList;
import programacioncapas.dao.UsuarioDAO;
import programacioncapas.entities.MiError;
import programacioncapas.entities.Usuario;

/**
 *
 * @author ALLAN
 */
public class UsuarioBO {

    /**
     *
     * @param usuario
     * @param reContra
     * @return
     */
    public boolean registrar(Usuario usuario, String reContra) {
        //Validar el usuario, campos requeridos, igualdad de contraseñas,
        //Formatos de fechas, cedulas, numeros, etc
        if (usuario.getEmail().isEmpty()) {
            throw new MiError("Correo requerido");
        }

        //TODO:Formato de correo???
        if (usuario.getContrasena().isEmpty()) {
            throw new MiError("Contraseña requerida");
        }

        if (!usuario.getContrasena().equals(reContra)) {
            throw new MiError("Contraseñas no coinciden");
        }

        //Llamar una capa de datos
        UsuarioDAO udao = new UsuarioDAO();
        System.out.println(MD5(usuario.getContrasena()));
        usuario.setContrasena(MD5(usuario.getContrasena()));
        if (usuario.getId() == 0) {
            return udao.insertar(usuario);
        } else {
            return udao.modificar(usuario);
        }
    }

    public LinkedList<Usuario> cargarTodo(String filtro) {
        UsuarioDAO udao = new UsuarioDAO();
        if (filtro.isEmpty()) {
            return udao.cargarTodo();
        } else {
            return udao.cargarFiltro(filtro);
        }
    }

    /**
     * Objetivo ???
     *
     * @param id ???
     * @return ??
     */
    public Usuario cargarID(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un usuario");
        }
        UsuarioDAO udao = new UsuarioDAO();
        return udao.cargarID(id);
    }

    public boolean borrar(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un usuario");
        }
        UsuarioDAO udao = new UsuarioDAO();
        return udao.eliminar(id);
    }

    public boolean autenficar(String email, String pass){
        //Validaciones
        UsuarioDAO udao = new UsuarioDAO();
        return udao.validar(email, MD5(pass));
    }
    
    private String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
