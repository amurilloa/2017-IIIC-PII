-- create database app; --Crea una base de datos
-- SQL
-- drop table usuario;
create table usuario(
    id serial primary key,
    email text not null unique,
    pass text not null, --Encriptar !!!
    telefono int --Sacar a otra tabla xD
)
-- insert, delete, update, select
select * from usuario;

insert into usuario(email, pass, telefono)
values ('allanmual@gmail.com','12345','85262638');

insert into usuario(email, pass, telefono)
values ('lineth.matamoros@gmail.com','54321','87788838');

insert into usuario(email, pass, telefono)
values ('jperez@hotmail.com','123123','75362364');

-- select * from usuario where email = 'allanmual@gmail.com'
-- select * from usuario where email like 'a%'

update usuario set pass = 11111 where id = 1as

