/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ALLAN
 * Created: nov 27, 2017
 */


select * from raza
CREATE TABLE public.raza
(
    id serial,
    raza text NOT NULL,
    activo boolean,
    PRIMARY KEY (id)
)

create table perro(
    id serial primary key, 
    nombre text not null unique, 
    id_raza int not null,
    descripcion text
    )
    
alter table perro 
add constraint fk_per_raz 
foreign key (id_raza) references raza(id)

insert into perro(nombre, id_raza, descripcion) values 
('Capitán', 2, 'Blanco y negro, pequeño')

insert into raza(raza, activo) values('Golden',false)
alter table raza add constraint unq_raza unique (raza);

ALTER TABLE perro add column activo boolean default true 
select * from perro


