/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capasforaneas.entities;

/**
 *
 * @author ALLAN
 */
public class Perro {

    private int id;
    private String nombre;
    private Raza raza;
    private String descripcion;
    private boolean activo;

    public Perro() {
    }

    public Perro(int id, String nombre, Raza raza, String descripcion, boolean activo) {
        this.id = id;
        this.nombre = nombre;
        this.raza = raza;
        this.descripcion = descripcion;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Raza getRaza() {
        return raza;
    }

    public void setRaza(Raza raza) {
        this.raza = raza;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Perro{" + "id=" + id + ", nombre=" + nombre + ", raza=" + raza + ", descripcion=" + descripcion + ", activo=" + activo + '}';
    }
    

}
