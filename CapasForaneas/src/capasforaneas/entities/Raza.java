/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capasforaneas.entities;

import java.util.Objects;

/**
 *
 * @author ALLAN
 */
public class Raza {

    private int id;
    private String raza;
    private boolean activo;

    public Raza() {
    }

    public Raza(int id, String raza, boolean activo) {
        this.id = id;
        this.raza = raza;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    //perro1.equals(perro2)

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Raza other = (Raza) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString() {
        return raza; //activo ? raza : raza + "(inactiva)";
    }

}
