/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capasforaneas.dao;

import capasforaneas.entities.MiError;
import capasforaneas.entities.Perro;
import capasforaneas.entities.Raza;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class PerroDAO {

    public boolean insertar(Perro perro) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into perro(nombre, id_raza, descripcion, activo) "
                    + "values (?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, perro.getNombre());
            stmt.setInt(2, perro.getRaza().getId());
            stmt.setString(3, perro.getDescripcion());
            stmt.setBoolean(4, perro.isActivo());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();// TODO: Eliminar esta  línea
            throw new MiError("No se pudo registrar el perro, favor intente nuevamente");
        }
    }

    public LinkedList<Perro> selectAll() {
        LinkedList<Perro> perros = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from perro";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                perros.add(cargarPerro(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar los perros, favor intente nuevamente");
        }

        return perros;
    }

    private Perro cargarPerro(ResultSet rs) throws SQLException {
        Perro perro = new Perro();
        perro.setId(rs.getInt("id"));
        perro.setNombre(rs.getString("nombre"));
        perro.setDescripcion(rs.getString("descripcion"));

        //Cargar la foranea
        RazaDAO rdao = new RazaDAO();
        perro.setRaza(rdao.cargarID(rs.getInt("id_raza")));

        perro.setActivo(rs.getBoolean("activo"));
        return perro;
    }

    public Perro selectByID(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from perro where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarPerro(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar el perro, favor intente nuevamente");
        }
        return null;
    }

}
