/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capasforaneas.dao;

import capasforaneas.entities.MiError;
import capasforaneas.entities.Raza;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class RazaDAO {

    public LinkedList<Raza> selectAll(boolean activo) {
        LinkedList<Raza> razas = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = activo ? "select * from raza where activo = true"
                    : "select * from raza ";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                razas.add(cargarRaza(rs));
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar las razas, favor intente nuevamente");
        }

        return razas;
    }

    private Raza cargarRaza(ResultSet rs) throws SQLException {
        Raza raza = new Raza();
        raza.setId(rs.getInt("id"));
        raza.setRaza(rs.getString("raza"));
        raza.setActivo(rs.getBoolean("activo"));
        return raza;
    }

    public Raza cargarID(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from raza where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarRaza(rs);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();// TODO: Eliminar esta  línea
            System.out.println(ex.getMessage());// TODO: Eliminar esta  línea
            throw new MiError("Problemas al cargar las razas, favor intente nuevamente");
        }
        return null;
    }
}
