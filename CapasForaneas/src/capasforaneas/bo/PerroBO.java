/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capasforaneas.bo;

import capasforaneas.dao.PerroDAO;
import capasforaneas.entities.MiError;
import capasforaneas.entities.Perro;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class PerroBO {

    private PerroDAO pdao;

    public PerroBO() {
        pdao = new PerroDAO();
    }

    public boolean registrar(Perro p) {
        //Validaciones correspondientes
        if (p.getNombre().isEmpty()) {
            throw new MiError("Nombre del perro requerido");
        }

        if (p.getRaza() == null) {
            throw new MiError("Raza requerida");
        }

        if (p.getRaza().getId() <= 0) {
            throw new MiError("Raza requerida");
        }

        return pdao.insertar(p);
    }

    public LinkedList<Perro> cargarTodos() {
        return pdao.selectAll();
    }

    public Perro getPerro(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar un perro");
        }
        return pdao.selectByID(id);
    }

}
