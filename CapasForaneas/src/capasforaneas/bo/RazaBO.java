/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capasforaneas.bo;

import capasforaneas.dao.RazaDAO;
import capasforaneas.entities.Perro;
import capasforaneas.entities.Raza;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class RazaBO {

    private RazaDAO rdao;

    public RazaBO() {
        rdao = new RazaDAO();
    }

    /**
     * Carga las razas
     *
     * @param activo true carga solo razas activas, false carga todas las razas
     * @return LinkedList de razas
     */
    public LinkedList<Raza> cargarRazas(boolean activo) {
        return rdao.selectAll(activo);
    }

    public LinkedList<Raza> cargarRazas() {
        return rdao.selectAll(false);
    }

    public LinkedList<Raza> cargarRazasActivas() {
        return rdao.selectAll(true);
    }

    /**
     * 
     * @param activo
     * @param perro
     * @return 
     */
    public LinkedList<Raza> cargarRazas(boolean activo, Perro perro) {
        LinkedList<Raza> razas = rdao.selectAll(activo);
        for (Raza raza : razas) {
            if (perro == null || raza.getId() == perro.getRaza().getId()) {
                return razas;
            }
        }
        Raza t = perro.getRaza();
        t.setRaza(t.getRaza()+ "(actual)");
        razas.add(t);
        return razas;
    }

}
