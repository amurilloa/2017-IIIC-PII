/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registropolicial;

import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Persona {

    private int id;
    private String nombre;
    private String apellido;
    private LinkedList<Perfil> perfiles;
    private LinkedList<Lugar> lugaresFrec;
    private Foto foto;

    public Persona() {
        perfiles = new LinkedList<>();
        lugaresFrec = new LinkedList<>();
    }

    public Persona(int id, String nombre, String apellido, Foto foto) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        perfiles = new LinkedList<>();
        lugaresFrec = new LinkedList<>();
        this.foto = foto;
    }

    /**
     * Agrega un lugar frecuente a la persona
     *
     * @param l Lugar que se desea agregar
     */
    public void agregarLugar(Lugar l) {
        lugaresFrec.add(l);
    }

    /**
     * Crea un nuevo perfil para la persona
     *
     * @param tipo int Tipo del perfil
     * @param desc String descripción del perfil
     */
    public void agregarPerfil(int tipo, String desc) {
        perfiles.add(new Perfil(tipo, desc));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LinkedList<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(LinkedList<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public LinkedList<Lugar> getLugaresFrec() {
        return lugaresFrec;
    }

    public void setLugaresFrec(LinkedList<Lugar> lugaresFrec) {
        this.lugaresFrec = lugaresFrec;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

}
