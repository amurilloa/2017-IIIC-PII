/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registropolicial;

import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private LinkedList<Persona> personas;
    private LinkedList<Lugar> lugares;
    private int id;

    public Logica() {
        personas = new LinkedList<>();
        lugares = new LinkedList<>();
        lugaresDemo();
        id = 1;
    }

    public void registrar(Persona p) {
        personas.add(p);
    }

    public void agregarLugar(Lugar l) {
        lugares.add(l);
    }

    public LinkedList<Lugar> getLugares() {
        return lugares;
    }

    private void lugaresDemo() {

        if (lugares.size() == 0) {
            Lugar l = new Lugar("Canasta Básica", "El Carmen", "2460-1234");
            agregarLugar(l);
            l = new Lugar("Cachos", "Terminal", "2460-1434");
            agregarLugar(l);
            l = new Lugar("Est. Carlos Ugalde", "San Martin", "2460-3323");
            agregarLugar(l);
        }
    }

    public int nextID() {
        return id++;
    }

    public int getId() {
        return id;
    }

    public Persona buscarID(int id) {
        for (Persona persona : personas) {
            if (persona.getId() == id) {
                return persona;
            }
        }
        return null;
    }
}
