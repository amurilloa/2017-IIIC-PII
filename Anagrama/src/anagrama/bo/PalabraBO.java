/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrama.bo;

import anagrama.dao.PalabraDAO;
import anagrama.entities.Palabra;

/**
 *
 * @author ALLAN
 */
public class PalabraBO {

    private PalabraDAO pdao;

    public PalabraBO() {
        pdao = new PalabraDAO();
    }

    public Palabra getPalabra() {
        return pdao.obtenerPalabra();
    }

    public int getTotal() {
        return pdao.getCount();
    }

    public boolean existe(String palabra) {
        if (palabra.isEmpty()) {
            return false;
        } 
        return pdao.existe(palabra);
    }

}
