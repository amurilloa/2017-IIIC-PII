/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrama.bo;

import anagrama.dao.PalabraDAO;
import anagrama.entities.Palabra;

/**
 *
 * @author ALLAN
 */
public class JuegoBO {

    private int totalPalabras;
    private int puntos;
    private int palabrasMostradas;
    private Palabra actual;
    private PalabraBO pbo;

    public JuegoBO() {
        pbo = new PalabraBO();
    }

    /**
     * 
     */
    public void iniciar() {
        //Calcular el toltal de palabras
        puntos = 0;
        palabrasMostradas = 1;
        actual = pbo.getPalabra();
        totalPalabras = pbo.getTotal();
        desordenarPalabra();
    }

    /**
     * 
     */
    private void desordenarPalabra() {
        String nueva = "";
        char[] pal = actual.getPalabra().toCharArray();
        while (nueva.length() < pal.length) {
            int r = (int) (Math.random() * pal.length);
            if (pal[r] != '-') {
                nueva += pal[r];
                pal[r] = '-';
            }
            if (nueva.length() == pal.length && pbo.existe(nueva)) {
                System.out.println(actual.getPalabra());
                System.out.println(nueva);
                System.out.println("nuevo intento");
                nueva = "";
                pal = actual.getPalabra().toCharArray();
            }
        }
        actual.setPalabra(nueva);
    }

    public Palabra getActual() {
        return actual;
    }

    public int getTotalPalabras() {
        return totalPalabras;
    }

    public int getPuntos() {
        return puntos;
    }

    public int getPalabrasMostradas() {
        return palabrasMostradas;
    }

    /**
     * 
     * @param palabra 
     */
    public void siguienteTurno(String palabra) {
        if (confirmarLetras(palabra) && pbo.existe(palabra)) {
            puntos++;
        } else {
            puntos--;
        }
        actual = pbo.getPalabra();
        desordenarPalabra();
        palabrasMostradas++;
    }

    /**
     * 
     * @param palabra
     * @return 
     */
    private boolean confirmarLetras(String palabra) {
        for (char letra : palabra.toCharArray()) {
            if (!actual.getPalabra().contains(String.valueOf(letra))) {
                return false;
            }
        }
        return true;
    }

}
