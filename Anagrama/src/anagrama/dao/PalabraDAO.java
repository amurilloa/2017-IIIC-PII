/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrama.dao;

import anagrama.entities.MiError;
import anagrama.entities.Palabra;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ALLAN
 */
public class PalabraDAO {

    public Palabra obtenerPalabra() {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select id, palabra from palabra order by  random() limit 1";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return cargarPalabra(rs);
            }
            return null;
        } catch (Exception ex) {
            throw new MiError("Problemas al obtener la palabra");
        }
    }

    private Palabra cargarPalabra(ResultSet rs) throws SQLException {
        Palabra p = new Palabra();
        p.setId(rs.getInt("id"));
        p.setPalabra(rs.getString("palabra"));
        return p;
    }

    public int getCount() {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select count(*) from palabra";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            return rs.next() ? rs.getInt(1) : 0;
        } catch (Exception ex) {
            throw new MiError("Problemas al obtener la palabra");
        }
    }

    public boolean existe(String palabra) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select count(*) from palabra where palabra = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, palabra);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
            return false;
        } catch (Exception ex) {
            throw new MiError("Problemas al obtener la palabra");
        }
    }

}
