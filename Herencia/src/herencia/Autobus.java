/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author ALLAN
 */
public class Autobus extends Vehiculo {

    int cantPasajeros;

    public Autobus() {
    }

    public int getCantPasajeros() {
        return cantPasajeros;
    }

    public void setCantPasajeros(int cantPasajeros) {
        this.cantPasajeros = cantPasajeros;
    }

    @Override
    public String toString() {
        return "Autobus{" + "cantPasajeros=" + cantPasajeros + '}';
    }

}
