/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author ALLAN
 */
public class CamionCompartimientos extends Camion {

    int cantCompar;

    public CamionCompartimientos() {
    }

    public double capaCompar() {
        return carga / cantCompar;
    }

    public int getCantCompar() {
        return cantCompar;
    }

    public void setCantCompar(int cantCompar) {
        this.cantCompar = cantCompar;
    }

    @Override
    public String toString() {
        return marca + " " + cantCompar;
    }

}
